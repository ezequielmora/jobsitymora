﻿using Domain.Models;

namespace ChatJobsityTask.Services
{
    public interface IBotConnectionService
    {
        ApiResponse BotDetection(string message);
    }
}
