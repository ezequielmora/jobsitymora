using NUnit.Framework;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChatBot.Test
{
    public class Tests
    {
        [Test]
        public async Task GetValue_Ok()
        {
            var uri = new Uri("https://localhost:44302/api/ChatBot/GetStock?inputCode=aapl.us");
            var client = new HttpClient { MaxResponseContentBufferSize = 256000 };

            HttpResponseMessage response = await client.GetAsync(uri);
            var sucess = response.IsSuccessStatusCode;
            Assert.True(sucess);
        }

        [Test]
        public async Task GetValue_Error()
        {
            var uri = new Uri("https://localhost:44302/api/ChatBot/GetStock?inputCode=");
            var client = new HttpClient { MaxResponseContentBufferSize = 256000 };

            HttpResponseMessage response = await client.GetAsync(uri);
            var sucess = response.IsSuccessStatusCode;
            Assert.False(sucess);
        }
    }
}