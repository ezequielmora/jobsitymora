﻿using System;
using ChatBot.Services;
using Microsoft.AspNetCore.Mvc;
using Domain.Models;

namespace ChatBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatBotController : ControllerBase
    {
        private IBotService retrieveService;

        public ChatBotController(IBotService serviceInstace)
        {
            retrieveService = serviceInstace;
        }

        [HttpGet]
        [Route("GetStock")]
        public ActionResult<Stock> GetStock(string inputCode)
        {
            try
            {
                var response = retrieveService.GetStock(inputCode);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}