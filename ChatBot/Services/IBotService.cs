﻿using Domain.Models;

namespace ChatBot.Services
{
    public interface IBotService
    {
        Stock GetStock(string inputCode);
    }
}
