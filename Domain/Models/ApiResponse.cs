﻿namespace Domain.Models
{
    public enum IsSuccessful
    {
        Fail,
        Command,
        Help
    }

    public class ApiResponse
    {
        public IsSuccessful IsSuccessful;
        public bool Detected { get; set; }
        public string ErrorMessage { get; set; }
        public string Symbol { get; set; }
        public float ClosePrice { get; set; }
    }
}
